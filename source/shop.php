﻿<!doctype html>

<?php
	include 'database.php';
?>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cửa hàng || LuxuryStreetWear Shop</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		
		<!-- google fonts  -->
		<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700,300,800' rel='stylesheet' type='text/css'>

		<!-- all css here -->
		<!-- bootstrap v3.3.6 css -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- animate css -->
        <link rel="stylesheet" href="css/animate.css">
		<!-- jquery-ui.min css -->
        <link rel="stylesheet" href="css/jquery-ui.min.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="css/meanmenu.min.css">
		<!-- owl.carousel css -->
        <link rel="stylesheet" href="css/owl.carousel.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="lib/css/nivo-slider.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<!-- responsive css -->
        <link rel="stylesheet" href="css/responsive.css">
		<!-- modernizr js -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <header class="header-pos">	
			<div class="header-bottom-area">
				<div class="container">
					<div class="inner-container">
						<div class="row">
							<div class="col-md-2 col-sm-4 col-xs-5">
								<div class="logo">
									<a href="index.php"><img src="img/streetwear.png" alt="logo header" /></a>
								</div>
							</div>
							<div class="col-md-8 hidden-xs hidden-sm">
								<div class="main-menu">
									<nav>
										<ul>						
											<li><a href="about.html">Về Chúng Tôi</a></li>
											<li class="static"><a href="shop.html">Cửa hàng</a>
												<div class="mega-menu">
													<div class="mega-left">
														<span>
															<a href="shop.html" class="mega-title">Giày </a>
															<a href="shop.html">Nike</a>
															<a href="shop.html">Adidas</a>
															<a href="shop.html">Rick Owen</a>
														</span>
														<span>
															<a href="shop.html" class="mega-title">Quần áo </a>
															<a href="shop.html">Áo khoác</a>
															<a href="shop.html">Áo sơ mi</a>
															<a href="shop.html">Áo phông </a>
														</span>
														<span>
															<a href="shop.html" class="mega-title">Phụ kiện </a>
															<a href="shop.html">Mũ</a>
															<a href="shop.html">Vòng tay</a>
															<a href="shop.html">Găng tay</a>
															<a href="shop.html">Khuyên</a>
														</span>
			
													</div>
													<div class="mega-right">
														<span class="mega-menu-img">
															<a href="shop.html"><img alt="" src="img/banner3.jpg"></a>
														</span>
													</div>
												</div>										
											</li>
											<li><a href="contact.html">liên hệ</a></li>
											<li><a href="login.php">Đăng nhập</a></li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</header>

		<div class="breadcrumb-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="breadcrumb-list">
							<h1>Danh mục</h1>
							<ul>
								<li><a href="index.php">Trang chủ</a> <span class="divider">|</span></li>
								<li><a href="shop.php">Cửa hàng</a></li>
							</ul>							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="shop-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<aside class="widget widget-categories">
							<h3 class="sidebar-title">Bạn có thể thích</h3>
							<?php
									$sql = "SELECT * FROM product ORDER BY RAND() LIMIT 3";
									mysqli_set_charset($dbConnect, 'UTF8');
									$result = mysqli_query($dbConnect,$sql);
									if (isset($result) && mysqli_num_rows($result) > 0) {
									while ($row = mysqli_fetch_assoc($result)) {
										$rp= $row['price'] - $row['discount'];
										echo "
										<div class=\"recent-product\">
											<div class=\"single-product\">
												<div class=\"product-img\">
													<a href=\"product-details.php?id=${row['id']}&view=${row['view']}\">
														<img src=\"${row['image_link']}\" alt=\"\" />
														<img class=\"secondary-img\" src=\"${row['image_link']}\" alt=\"\" />
													</a>
												</div>
												<div class=\"product-content\">
													<h3><a href=\"product-details.php?id=${row['id']}&view=${row['view']}\">${row['name']}</a></h3>
													<div class=\"price\">
														<span>${rp}$</span>
														<span class=\"old\">${row['price']}$</span>
													</div>
												</div>
											</div>
										</div>
										";
									}
									}
									?>								
						</aside>

						<aside class="widget product-tag">
							<h3 class="sidebar-title">Tags</h3>
							<ul>
								<li><a href="#">Top</a></li>
								<li><a href="#">Fashion</a></li>
								<li><a href="#">Collection</a></li>
								<li><a href="#">Women</a></li>
								<li><a href="#">men</a></li>
								<li><a href="#">gallery</a></li>
								<li><a href="#">new</a></li>
								<li><a href="#">Collection men</a></li>
								<li><a href="#">Top</a></li>
								<li><a href="#">Fashion</a></li>
								<li><a href="#">Collection</a></li>										
								<li><a href="#">best</a></li>
								<li><a href="shop.php">cloth</a></li>
							</ul>
						</aside>

						<aside class="widget sale-off hidden-sm">
							<div class="sale-off-carosel">
								<?php
								$sql = "SELECT * FROM product ORDER BY discount DESC LIMIT 3";
								mysqli_set_charset($dbConnect, 'UTF8');
								$result = mysqli_query($dbConnect,$sql);
								if (isset($result) && mysqli_num_rows($result) > 0) {
								while ($row = mysqli_fetch_assoc($result)) {
									echo "
											<div class=\"single-sale\">
												<a href=\"product-details.php?id=${row['id']}&view=${row['view']}\">
													<img src=\"${row['image_link']}\" alt=\"\" />
													<h2>Giảm Giá</h2>									
												</a>
											</div>
										";
								}
								}
								?>
							</div>
						</aside>							
					</div>

					<div class="col-md-9 col-sm-12 col-xs-12">
						<div class="shop-content">					
							<div class="short-by">
								<span class="sorting-show"> Sắp xếp theo:</span>
								
								<select>
									<option>
										Xem nhiều nhất
									</option>
									<option value="saab">Mặc định</option>
									<option value="audi">Giá thấp đến cao</option>
									<option value="audi">Giá cao đến thấp</option>
								</select>					
							</div>
					
							<div class="clear"></div>

							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="home">
									<div class="row">
									<?php
									$sql = "SELECT * FROM product";
									mysqli_set_charset($dbConnect, 'UTF8');
									$result = mysqli_query($dbConnect,$sql);
									if (isset($result) && mysqli_num_rows($result) > 0) {
									while ($row = mysqli_fetch_assoc($result)){
										$rp= $row['price'] - $row['discount'];
										echo "
										<div class=\"col-md-4 col-sm-4\">
											<div class=\"single-product\">
												<div class=\"product-img\">
													<a href=\"single-product.html\">
														<img src=\"${row['image_link']}\" alt=\"\" />
														<img class=\"secondary-img\" src=\"${row['image_link']}\" alt=\"\" />
													</a>
													<span class=\"tag-line\">${row['view']}</span>
													<div class=\"product-action\">
														<div class=\"button-cart\">
															<button><i class=\"fa fa-shopping-cart\"></i> Thêm vào giỏ</button>
														</div>
													</div>
												</div>
												<div class=\"product-content\">
													<h3><a href=\"single-product.html\">${row['name']}</a></h3>
													<div class=\"price\">
														<span>${rp}$</span>
														<span class=\"old\">${row['price']}</span>
													</div>
												</div>
											</div>
										</div>
										";
										}
										}
									?>				
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<footer>
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="footer-logo">
								<img src="img/streetwear.png" alt="streetwear" />
								<p>LuxuryStreetWear mang đến cho bạn những bộ sưu tập mới nhất làm điên đảo toàn cầu <br /> Hãy đến với chúng tôi để bắt kịp với xu thế thời trang thế giới</p>
								<div class="widget-icon">
									<a href="https://www.facebook.com/ducphu96bn"><i class="fa fa-facebook"></i></a>
									<a href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-top-area">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="footer-widget">
								<h3>Liên hệ</h3>
								<ul class="footer-contact">
									<li>
										<i class="fa fa-map-marker"> </i>
										Địa chỉ: 8 Mã Mây, Quận Hoàn Kiếm, Hà Nội
									</li>
									<li>
										<i class="fa fa-envelope"> </i>	
										Email: luxurystreetwear@shop.com
									</li>
									<li>
										<i class="fa fa-phone"> </i>
										Điện thoại: 0989.899.998
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 hidden-sm">
							<div class="footer-widget">
								<h3>Tài khoản của tôi</h3>
								<ul class="footer-menu">
									<li><a href="#">Trạng thái đơn hàng</a></li>
									<li><a href="login.php">Đăng kí/Đăng nhập</a></li>
									<li><a href="#">Danh sách mơ ước</a></li>
									<li><a href="#">Kiểu thanh toán</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="footer-widget">
								<h3>Về chúng tôi</h3>
								<ul class="footer-menu">
									<li><a href="#">Giao hàng</a></li>
									<li><a href="#">Thanh toán</a></li>
									<li><a href="#">Thông tin pháp lí</a></li>
									<li><a href="#">Về chúng tôi</a></li>
									<li><a href="#">Liên hệ với chúng tôi</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="footer-widget">
								<h3>Tags</h3>
								<div class="product-tag">
									<ul>
										<li><a href="#">Top</a></li>
										<li><a href="#">Fashion</a></li>
										<li><a href="#">Collection</a></li>
										<li><a href="#">Women</a></li>
										<li><a href="#">men</a></li>
										<li><a href="#">gallery</a></li>
										<li><a href="#">new</a></li>
										<li><a href="#">Collection men</a></li>
										<li><a href="#">Top</a></li>
										<li><a href="#">Fashion</a></li>
										<li><a href="#">Collection</a></li>										
										<li><a href="#">best</a></li>
										<li><a href="shop.html">cloth</a></li>
									</ul>
								</div>
							</div>
						</div>					
					</div>
				</div>
			</div>
			<div class="footer-bottom-area">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="payment-img">
								<img src="img/payment.png" alt="" />
							</div>
						</div>
					</div>
				</div>
			</div>	
		</footer>
		



		<!-- jquery latest version -->
        <script src="js/vendor/jquery-1.12.0.min.js"></script>
		<!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
        <script src="js/owl.carousel.min.js"></script>
		<!-- meanmenu js -->
        <script src="js/jquery.meanmenu.js"></script>
		<!-- jquery-ui js -->
        <script src="js/jquery-ui.min.js"></script>
		<!-- nivo.slider js -->
		<script src="lib/js/jquery.nivo.slider.pack.js"></script>		
		<script src="lib/js/nivo-active.js"></script>		
		<!-- wow js -->
        <script src="js/wow.min.js"></script>
		<!-- plugins js -->
        <script src="js/plugins.js"></script>
		<!-- main js -->
        <script src="js/main.js"></script>
    </body>
</html>
