<!doctype html>

<?php
	include 'database.php';
	if($dbConnect) {
		$id = $_GET['id'];
		$view = $_GET['view'];
		$dbquery="UPDATE product SET view = $view + 1 WHERE id = $id";
		$result = mysqli_query($dbConnect,$dbquery);
		if(isset($result)){

		}
	}
?>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Chi tiết sản phẩm || LuxuryStreetWear Shop</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
  
		<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700,300,800' rel='stylesheet' type='text/css'>

		<!-- all css here -->
		<!-- bootstrap v3.3.6 css -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- animate css -->
        <link rel="stylesheet" href="css/animate.css">
		<!-- jquery-ui.min css -->
        <link rel="stylesheet" href="css/jquery-ui.min.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="css/meanmenu.min.css">
		<!-- owl.carousel css -->
        <link rel="stylesheet" href="css/owl.carousel.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="lib/css/nivo-slider.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<!-- responsive css -->
        <link rel="stylesheet" href="css/responsive.css">
		<!-- modernizr js -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <header class="header-pos">
			<div class="header-bottom-area">
				<div class="container">
					<div class="inner-container">
						<div class="row">
							<div class="col-md-2 col-sm-4 col-xs-5">
								<div class="logo">
									<a href="index.php"><img src="img/streetwear.png" alt="logo header" /></a>
								</div>
							</div>
							<div class="col-md-8 hidden-xs hidden-sm">
								<div class="main-menu">
									<nav>
										<ul>
											<li><a href="index.php">Trang chủ</a></li>
											<li><a href="about.html">Về chúng tôi</a></li>
											<li class="static"><a href="shop.html">Cửa hàng</a>
												<div class="mega-menu">
													<div class="mega-left">
														<span>
															<a href="shop.html" class="mega-title">Giày </a>
															<a href="shop.html">Nike</a>
															<a href="shop.html">Adidas</a>
															<a href="shop.html">Rick Owen</a>
														</span>
														<span>
															<a href="shop.html" class="mega-title">Quần áo </a>
															<a href="shop.html">Áo khoác</a>
															<a href="shop.html">Áo sơ mi</a>
															<a href="shop.html">Áo phông </a>
														</span>
														<span>
															<a href="shop.html" class="mega-title">Phụ kiện </a>
															<a href="shop.html">Mũ</a>
															<a href="shop.html">Vòng tay</a>
															<a href="shop.html">Găng tay</a>
															<a href="shop.html">Khuyên</a>
														</span>
													</div>
													<div class="mega-right">
														<span class="mega-menu-img">
															<a href="shop.html"><img alt="" src="img/banner3.jpg"></a>
														</span>
													</div>
												</div>										
											</li>
											<li><a href="login.php">đăng nhập</a></li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div class="breadcrumb-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="breadcrumb-list">
							<h1>Chi tiết sản phẩm</h1>
							<ul>
								<li><a href="index.php">Trang chủ</a> <span class="divider">|</span></li>
								<li>Chi tiết sản phẩm</li>
							</ul>							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="shop-area">
			<div class="container">
				<div class="row">
				<?php

					$id=$_GET['id'];
					$sql = "SELECT * FROM product WHERE id = $id";
					mysqli_set_charset($dbConnect, 'UTF8');
					$result = mysqli_query($dbConnect,$sql);
					if (isset($result) && mysqli_num_rows($result) > 0) {
					while ($row = mysqli_fetch_assoc($result)) {
						$rp= $row['price'] - $row['discount'];
						echo "
						<div class=\"col-md-9\">
							<div class=\"row\">
								<div class=\"col-md-5 col-sm-5 col-xs-12\">
									<div class=\"single-pro-tab-content\">
										<div class=\"tab-content\">
											<div role=\"tabpanel\" class=\"tab-pane active\" id=\"home\"><a href=\"#\"><img class=\"zoom\" src=\"${row['image_link']}\" data-zoom-image=\"${row['image_link']}\" alt=\"\"></a>
											</div>				
										</div>								
									</div>
								</div>	
								<div class=\"col-md-7 col-sm-7 col-xs-12 shop-list shop-details\">								
									<div class=\"product-content\">
										<h3><a href=\"product-details.php?id=${row['id']}&view=${row['view']}\">${row['name']}</a></h3>
										<div class=\"price\">
											<span>${rp} $</span>
											<span class=\"old\">${row['price']} $</span>
										</div>
										<p>${row['content']}</p>
										<div class=\"product-action\">
											<div class=\"cart-plus\">
												<form action=\"#\">
													<div class=\"cart-plus-minus\"><input type=\"text\" value=\"1\" /></div>
												</form>
											</div>
											<div class=\"button-cart\">
												<button><i class=\"fa fa-shopping-cart\"></i> Thêm vào giỏ hàng</button>
											</div>
										</div>	
										<div class=\"product-share\">
											<label>Chia sẻ:</label>
											<span>
												<a href=\"#\"><i class=\"fa fa-facebook\"></i></a>
											</span>
										</div>
									</div>							
								</div>
							</div>
							<div class=\"row\">
								<div class=\"col-md-12 col-sm-12\">
									<div class=\"product-tabs\">
											<div>
											  <ul class=\"pro-details-tab\" role=\"tablist\">
												<li role=\"presentation\" class=\"active\"><a href=\"#tab-desc\" aria-controls=\"tab-desc\" role=\"tab\" data-toggle=\"tab\">Chi tiết</a></li>
											  </ul>
											</div>  
											<div class=\"tab-content\">
												<div role=\"tabpane\" class=\"tab-pane active\" id=\"tab-desc\">
													<div class=\"product-tab-desc\">
														<p>${row['content']}</p>
														<ul id=\"product-desc-t\">
															<li>${row['details']}</li>
														</ul>
													</div>
												</div>
											</div>
										</div>						
									</div>							
								</div>
							</div>
						</div>";
					}
					}
				?>

				<div class="row">
								<div class="col-md-12">
									<div class="section-title">
										<h2>Được xem nhiều nhất</h2>
										<div class="title-icon">
											<span><i class="fa fa-angle-left"></i> <i class="fa fa-angle-right"></i></span>
										</div>
									</div>							
								</div>
								<div class="related-curosel">

				<?php
					$sql = "SELECT * FROM product ORDER BY view DESC LIMIT 4";
					mysqli_set_charset($dbConnect, 'UTF8');
					$result = mysqli_query($dbConnect,$sql);
					if (isset($result) && mysqli_num_rows($result) > 0) {
					while ($row = mysqli_fetch_assoc($result)) {
						$rp= $row['price'] - $row['discount'];
						echo "
							




								<div class=\"col-md-12\">
										<div class=\"single-product\">
											<div class=\"product-img\">
												<a href=\"product-details.php?id=${row['id']}&view=${row['view']}\">
													<img src=\"${row['image_link']}\" alt=\"\" />
													<img class=\"secondary-img\" src=\"${row['image_link']}\" alt=\"\" />
												</a>
												<span class=\"tag-line\">${row['view']} view</span>
												<div class=\"product-action\">
													<div class=\"button-cart\">
														<button><i class=\"fa fa-shopping-cart\"></i> Thêm vào giỏ hàng</button>
													</div>
												</div>
											</div>
											<div class=\"product-content\">
												<h3><a href=\"product-details.php?id=${row['id']}&view=${row['view']}\">${row['name']}</a></h3>
												<div class=\"price\">
													<span>${rp} $</span>
													<span class=\"old\">${row['price']} $</span>
												</div>
											</div>
										</div>
									</div>		



															
							
							";
					}
					}
				?>
					</div>							
							</div>		
				</div>
			</div>
		</div>

		<footer>
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="footer-logo">
								<img src="img/streetwear.png" alt="streetwear" />
								<p>LuxuryStreetWear mang đến cho bạn những bộ sưu tập mới nhất làm điên đảo toàn cầu <br /> Hãy đến với chúng tôi để bắt kịp với xu thế thời trang thế giới</p>
								<div class="widget-icon">
									<a href="https://www.facebook.com/ducphu96bn"><i class="fa fa-facebook"></i></a>
									<a href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer-top-area start -->
			<div class="footer-top-area">
				<div class="container">
					<div class="row">
						<!-- footer-widget start -->
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="footer-widget">
								<h3>Liên hệ</h3>
								<ul class="footer-contact">
									<li>
										<i class="fa fa-map-marker"> </i>
										Địa chỉ: 8 Mã Mây, Quận Hoàn Kiếm, Hà Nội
									</li>
									<li>
										<i class="fa fa-envelope"> </i>	
										Email: luxurystreetwear@shop.com
									</li>
									<li>
										<i class="fa fa-phone"> </i>
										Điện thoại: 0989.899.998
									</li>
								</ul>
							</div>
						</div>
						<!-- footer-widget end -->					
						<!-- footer-widget start -->
						<div class="col-lg-3 col-md-3 hidden-sm">
							<div class="footer-widget">
								<h3>Tài khoản của tôi</h3>
								<ul class="footer-menu">
									<li><a href="#">Trạng thái đơn hàng</a></li>
									<li><a href="#">Đăng ký/Đăng nhập</a></li>
									<li><a href="#">Danh sách mơ ước</a></li>
									<li><a href="#">Kiểu thanh toán</a></li>
								</ul>
							</div>
						</div>
						<!-- footer-widget end -->
						<!-- footer-widget start -->
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="footer-widget">
								<h3>Về chúng tôi</h3>
								<ul class="footer-menu">
									<li><a href="#">Giao hàng</a></li>
									<li><a href="#">Thanh toán</a></li>
									<li><a href="#">Thông tin pháp lí</a></li>
									<li><a href="#">Về chúng tôi</a></li>
									<li><a href="#">Liên hệ với chúng tôi</a></li>
								</ul>
							</div>
						</div>
						<!-- footer-widget end -->
						<!-- footer-widget start -->
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="footer-widget">
								<h3>Product tags</h3>
								<div class="product-tag">
									<ul>
										<li><a href="#">Top</a></li>
										<li><a href="#">Fashion</a></li>
										<li><a href="#">Collection</a></li>
										<li><a href="#">Women</a></li>
										<li><a href="#">men</a></li>
										<li><a href="#">gallery</a></li>
										<li><a href="#">new</a></li>
										<li><a href="#">Collection men</a></li>
										<li><a href="#">Top</a></li>
										<li><a href="#">Fashion</a></li>
										<li><a href="#">Collection</a></li>										
										<li><a href="#">best</a></li>
										<li><a href="shop.html">cloth</a></li>
									</ul>
								</div>
							</div>
						</div>					
					</div>
				</div>
			</div>		
		</footer>

		<!-- all js here -->
		<!-- jquery latest version -->
        <script src="js/vendor/jquery-1.12.0.min.js"></script>
		<!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
        <script src="js/owl.carousel.min.js"></script>
		<!-- meanmenu js -->
        <script src="js/jquery.meanmenu.js"></script>
		<!-- jquery-ui js -->
        <script src="js/jquery-ui.min.js"></script>
		<!-- elevateZoom js -->
        <script src="js/jquery.elevateZoom-3.0.8.min.js"></script>
		<!-- nivo.slider js -->
		<script src="lib/js/jquery.nivo.slider.pack.js"></script>		
		<script src="lib/js/nivo-active.js"></script>		
		<!-- wow js -->
        <script src="js/wow.min.js"></script>
		<script>
			$(".zoom").elevateZoom();
		</script>
		<!-- plugins js -->
        <script src="js/plugins.js"></script>
		<!-- main js -->
        <script src="js/main.js"></script>
    </body>
</html>
