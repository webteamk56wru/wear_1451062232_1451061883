﻿<?php
	session_start();
?>

<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Đăng Nhập || LuxuryStreetWear Shop</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		
		<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700,300,800' rel='stylesheet' type='text/css'>

		<!-- bootstrap v3.3.6 css -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- animate css -->
        <link rel="stylesheet" href="css/animate.css">
		<!-- jquery-ui.min css -->
        <link rel="stylesheet" href="css/jquery-ui.min.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="css/meanmenu.min.css">
		<!-- owl.carousel css -->
        <link rel="stylesheet" href="css/owl.carousel.css">
		<!-- nivo-slider css -->
        <link rel="stylesheet" href="lib/css/nivo-slider.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<!-- responsive css -->
        <link rel="stylesheet" href="css/responsive.css">
		<!-- modernizr js -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <header class="header-pos">
			<div class="header-bottom-area">
				<div class="container">
					<div class="inner-container">
						<div class="row">
							<div class="col-md-2 col-sm-4 col-xs-5">
								<div class="logo">
									<a href="index.php"><img src="img/streetwear.png" alt="logo" /></a>
								</div>
							</div>
							<div class="col-md-8 hidden-xs hidden-sm">
								<div class="main-menu">
									<nav>
										<ul>
											<li><a href="index.php">Trang chủ</a>
											<li><a href="about.html">Về chúng tôi</a></li>
											<li class="static"><a href="shop.html">cửa hàng</a>
												<div class="mega-menu">
													<div class="mega-left">
													<span>
															<a href="shop.html" class="mega-title">Giày </a>
															<a href="shop.html">Nike</a>
															<a href="shop.html">Adidas</a>
															<a href="shop.html">Rick Owen</a>
														</span>
														<span>
															<a href="shop.html" class="mega-title">Quần áo </a>
															<a href="shop.html">Áo khoác</a>
															<a href="shop.html">Áo sơ mi</a>
															<a href="shop.html">Áo phông </a>
														</span>
														<span>
															<a href="shop.html" class="mega-title">Phụ kiện </a>
															<a href="shop.html">Mũ</a>
															<a href="shop.html">Vòng tay</a>
															<a href="shop.html">Găng tay</a>
															<a href="shop.html">Khuyên</a>
														</span>
													</div>
													<div class="mega-right">
														<span class="mega-menu-img">
															<a href="shop.html"><img alt="" src="img/banner3.jpg"></a>
														</span>
													</div>
												</div>										
											</li>							
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div class="breadcrumb-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="breadcrumb-list">
							<h1>Đăng nhập</h1>
							<ul>
								<li><a href="index.php">Trang chủ</a> <span class="divider">|</span></li>
								<li>Đăng nhập</li>
							</ul>							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="login-area">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="login-content">
							<h2 class="login-title">Đăng nhập</h2>
							<p>Chào mừng bạn đến với LuxuryStreetWear Shop</p>
							<?php
 
							$host="localhost"; // luôn luôn là localhost
							$username="root"; // user của mysql
							$password=""; // Mysql password
							$db_name="streetwear"; // tên database
							$tbl_name="admin"; // tên table

							if(isset($_POST['ok']))
							{
							$myusername=$mypassword="";
								if($_POST['username'] == NULL)
								{
								  	echo "Bạn chưa nhập vào tài khoản<br />";
								}
								else
								{
								  	$myusername=$_POST['username'];
								}
								
							 	if($_POST['password'] == NULL)
								{
								 	echo "Bạn chưa điền vào mật khẩu<br />";
								}
								 else
								{
								 	$mypassword=$_POST['password'];
								} 

								if($myusername && $mypassword)
								{
									// kết nối cơ sở dữ liệu
									$conn=mysqli_connect("$host", "$username", "$password","$db_name")or die("Không thể kết nối");

									// username và password được gửi từ form đăng nhập
									$myusername=$_POST['username'];
									$mypassword=$_POST['password'];
									 
									// Xử lý để tránh MySQL injection
									$myusername = stripslashes($myusername);
									$mypassword = stripslashes($mypassword);
									$myusername = mysqli_real_escape_string($conn,$myusername);
									$mypassword = mysqli_real_escape_string($conn,$mypassword);
									 
									$sql="SELECT * FROM $tbl_name WHERE username='$myusername' and password='$mypassword'";
									$result=mysqli_query($conn,$sql);
									$count=mysqli_num_rows($result);

									// nếu tài khoản trùng khớp thì sẽ trả về giá trị 1 cho biến $count
									if($count==1)
									{ 
										// Lúc này nó sẽ tự động gửi đến trang thông báo đăng nhập thành công
										$_SESSION['username']=$myusername;
										$_SESSION['password']=$mypassword;
										echo "Xin chào " . $myusername . ". Bạn đã đăng nhập thành công. <a href='index.php'>Về trang chủ</a>";
   										die();
									}
									else 
									{
										echo "Sai tên đăng nhập hoặc mật khẩu";
									}
								}
							}
							?>
							<form action='login.php' method='post'>
								<label>Tài khoản:</label> <input type='text' name='username'  /><br />
								<label>Mật khẩu:</label> <input type='password' name='password'  /><br />
								<input class='login-sub' type='submit' name='ok' value='Đăng Nhập' />
							</form>
						</div>
					</div>
					<div class="col-md-6">
						<div class="login-content login-margin">
							<h2 class="login-title">Tạo tài khoản mới</h2>
							<p>Đăng kí tài khoản LuxuryStreetWear Shop của bạn</p>
							<?php
								$username = "root";
								$password = "";
								$host = "localhost";
								$db_name = "streetwear";
								$tbl_name= "admin";
								 
								$conn = mysqli_connect($host,$username,$password,$db_name) or die("không thể kết nối tới database");
								mysqli_query($conn,"SET NAMES 'UTF8'");

								if (isset($_POST['singup'])) 
								{
							  		//lấy thông tin từ các form bằng phương thức POST
							  		//dùng hàm mssql_escape_string để chống sql injection
							  		$myusername = $_POST['username'] ? mysql_escape_string($_POST['username']) : '';
							  		$mypassword = $_POST['password'] ? md5($_POST['password']) : '';
							 		$myname = $_POST['name'] ? mysql_escape_string($_POST['name']) : '';
							 		$myemail = $_POST['email'] ? mysql_escape_string($_POST['email']) : '';
							  		//Kiểm tra điều kiện bắt buộc đối với các field không được bỏ trống
									if ($myusername == '' || $mypassword == '' || $myname == '' || $myemail== '') 
									{
										echo "bạn vui lòng nhập đầy đủ thông tin";
							  		}
							  		else
							  		{
							  			// Kiểm tra tài khoản đã tồn tại chưa
							  			$sql="SELECT * FROM $tbl_name WHERE username='$myusername'";
										$kt=mysqli_query($conn, $sql);
							 
											if(mysqli_num_rows($kt) > 0)
											{
												echo "Tài khoản đã tồn tại";
											}
											else
											{
												//thực hiện việc lưu trữ dữ liệu vào db
								    			$sql = "INSERT INTO $tbl_name (username,password,name,email) VALUES ('$myusername','$mypassword','$myname','$myemail')";
											    // thực thi câu $sql với biến $conn
							   					mysqli_query($conn,$sql);
											   	echo  "Bạn đã đăng ký thành công. <a href='index.php'>Về trang chủ</a> hoặc đăng nhập ngay bây giờ";
   										die();
											}			
									}
								}
							?>
							<form action="login.php" method="post">
								<label>Tài khoản:</label> <input type='text' id='username' name='username' /></br>
								<label>Mật khẩu:</label> <input type='password' id='password' name='password' /></br>
								<label>Họ Tên:</label> <input type='text' id='name' name='name' /></br>
								<label>Email:</label> <input type='text' id='email' name='email' /></br>
								<input class='login-sub' type='submit' name='singup' value='Đăng Ký' /></br>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="brand-area">
			<div class="container">
				<div class="brand-inner-container pad-60">
					<div class="row">
						<div class="brand-curosel">
							<div class="col-md-12">
								<div class="single-brand">
									<a href="#"><img src="img/brand1.png" alt="" /></a>
								</div>
							</div>
							<div class="col-md-12">
								<div class="single-brand">
									<a href="#"><img src="img/brand2.png" alt="" /></a>
								</div>
							</div>
							<div class="col-md-12">
								<div class="single-brand">
									<a href="#"><img src="img/brand3.png" alt="" /></a>
								</div>
							</div>
							<div class="col-md-12">
								<div class="single-brand">
									<a href="#"><img src="img/brand4.png" alt="" /></a>
								</div>
							</div>
							<div class="col-md-12">
								<div class="single-brand">
									<a href="#"><img src="img/brand5.png" alt="" /></a>
								</div>
							</div>
							<div class="col-md-12">
								<div class="single-brand">
									<a href="#"><img src="img/brand6.png" alt="" /></a>
								</div>
							</div>
							<div class="col-md-12">
								<div class="single-brand">
									<a href="#"><img src="img/brand7.png" alt="" /></a>
								</div>
							</div>
							<div class="col-md-12">
								<div class="single-brand">
									<a href="#"><img src="img/brand8.png" alt="" /></a>
								</div>
							</div>
							<div class="col-md-12">
								<div class="single-brand">
									<a href="#"><img src="img/brand9.png" alt="" /></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer>
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="footer-logo">
								<img src="img/streetwear.png" alt="" />
								<p>LuxuryStreetWear mang đến cho bạn những bộ sưu tập mới nhất làm điên đảo toàn cầu<br /> Hãy đến với chúng tôi để bắt kịp với xu thế thời trang thế giới.</p>
								<div class="widget-icon">
									<a href="https://wwww.facebook.com/ducphu96bn"><i class="fa fa-facebook"></i></a>
									<a href="https://wwww.instagram.com"><i class="fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-top-area">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="footer-widget">
								<h3>Liên hệ với chúng tôi:</h3>
								<ul class="footer-contact">
									<li>
										<i class="fa fa-map-marker"> </i>
										Địa chỉ: 8 Mã Mây, Quận Hoàn Kiếm, Hà Nội
									</li>
									<li>
										<i class="fa fa-envelope"> </i>	
										Email: luxurystreetwear@shop.com
									</li>
									<li>
										<i class="fa fa-phone"> </i>
										Điện thoại: 0989.899.998
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 hidden-sm">
							<div class="footer-widget">
								<h3>Tài khoản của tôi</h3>
								<ul class="footer-menu">
									<li><a href="#">Trạng thái đơn hàng</a></li>
									<li><a href="login.html">Đăng kí/Đăng nhập</a></li>
									<li><a href="#">Danh sách mơ ước</a></li>
									<li><a href="#">Kiểu thanh toán</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="footer-widget">
								<h3>Về chúng tôi</h3>
								<ul class="footer-menu">
									<li><a href="#">Giao hàng</a></li>
									<li><a href="#">Thanh toán</a></li>
									<li><a href="#">Thông tin pháp lí</a></li>
									<li><a href="about.html">Về chúng tôi</a></li>
									<li><a href="contact.html">Liên hệ với chúng tôi</a></li>
								</ul>
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="footer-widget">
								<h3>Tags</h3>
								<div class="product-tag">
									<ul>
										<li><a href="shop.html">Top</a></li>
										<li><a href="shop.html">Fashion</a></li>
										<li><a href="shop.html">Collection</a></li>
										<li><a href="shop.html">Women</a></li>
										<li><a href="shop.html">men</a></li>
										<li><a href="shop.html">gallery</a></li>
										<li><a href="shop.html">new</a></li>
										<li><a href="shop.html">Collection men</a></li>
										<li><a href="shop.html">Top</a></li>
										<li><a href="shop.html">Fashion</a></li>
										<li><a href="shop.html">Collection</a></li>										
										<li><a href="shop.html">best</a></li>
										<li><a href="shop.html">cloth</a></li>
									</ul>
								</div>
							</div>
						</div>						
					</div>
				</div>
			</div>		
		</footer>

        <script src="js/vendor/jquery-1.12.0.min.js"></script>
		
        <script src="js/bootstrap.min.js"></script>
		
        <script src="js/owl.carousel.min.js"></script>
		
        <script src="js/jquery.meanmenu.js"></script>
		
        <script src="js/jquery-ui.min.js"></script>
	
		<script src="lib/js/jquery.nivo.slider.pack.js"></script>		
		<script src="lib/js/nivo-active.js"></script>		
		
        <script src="js/wow.min.js"></script>
		
        <script src="js/plugins.js"></script>
		
        <script src="js/main.js"></script>
    </body>
</html>
