<!doctype html>


<?php
	include 'database.php';
?>


<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>LuxuryStreetWear Shop || Tin tức || Xu hướng thời trang mới nhất</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
       
		
		<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700,300,800' rel='stylesheet' type='text/css'>


        <link rel="stylesheet" href="css/bootstrap.min.css">
		
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/jquery-ui.min.css">
        <link rel="stylesheet" href="css/meanmenu.min.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="lib/css/nivo-slider.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <header class="header-pos">	
			<div class="header-bottom-area">
				<div class="container">
					<div class="inner-container">
						<div class="row">
							<div class="col-md-2 col-sm-4 col-xs-5">
								<div class="logo">
									<a href="index.php"><img src="img/streetwear.png" alt="logo header" /></a>
								</div>
							</div>
							<div class="col-md-8 hidden-xs hidden-sm">
								<div class="main-menu">
									<nav>
										<ul>						
											<li><a href="about.html">Về Chúng Tôi</a></li>
											<li class="static"><a href="shop.php">Cửa hàng</a>
												<div class="mega-menu">
													<div class="mega-left">
														<span>
															<a href="shop.php" class="mega-title">Giày </a>
															<a href="shop.php">Nike</a>
															<a href="shop.php">Adidas</a>
															<a href="shop.php">Rick Owen</a>
														</span>
														<span>
															<a href="shop.php" class="mega-title">Quần áo </a>
															<a href="shop.php">Áo khoác</a>
															<a href="shop.php">Áo sơ mi</a>
															<a href="shop.php">Áo phông </a>
														</span>
														<span>
															<a href="shop.php" class="mega-title">Phụ kiện </a>
															<a href="shop.php">Mũ</a>
															<a href="shop.php">Vòng tay</a>
															<a href="shop.php">Găng tay</a>
															<a href="shop.php">Khuyên</a>
														</span>
			
													</div>
													<div class="mega-right">
														<span class="mega-menu-img">
															<a href="shop.php"><img alt="" src="img/banner3.jpg"></a>
														</span>
													</div>
												</div>										
											</li>
											<li><a href="login.php">Đăng nhập</a></li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</header>
		<div class="slider-container">
			<div id="mainSlider" class="nivoSlider slider-image">
				<img src="img/slide1.jpg" alt="main slider" title="#htmlcaption1"/>
				<img src="img/slide2-1.jpg" alt="main slider" title="#htmlcaption2"/>
			</div>
			<div id="htmlcaption1" class="nivo-html-caption slider-caption-1">
				<div class="slider-progress"></div>	
				<div class="slide1-text">
					<div class="middle-text">
						<div class="cap-dec wow bounceInDown" data-wow-duration="0.9s" data-wow-delay="0s">
							<h3><b>Bộ sưu tập 2017</b></h3>
						</div>	
						<div class="cap-title wow bounceInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">
							<h1>Những set quần áo hot nhất hè này</h1>
						</div>
						<div class="cap-readmore wow bounceInUp" data-wow-duration="1.3s" data-wow-delay=".5s">
							<a href="#">Mua ngay</a>
						</div>	
					</div>	
				</div>						
			</div>
			<div id="htmlcaption2" class="nivo-html-caption slider-caption-2">
				<div class="slider-progress"></div>	
				<div class="slide1-text">
					<div class="middle-text">
						<div class="cap-dec wow bounceIn" data-wow-duration="0.7s" data-wow-delay="0s">
							<h3>Bộ sưu tập giày mới nhất hè này</h3>
						</div>	
						<div class="cap-title wow bounceIn" data-wow-duration="1s" data-wow-delay="0.2s">
							<h1>Những chiếc sneaker được nhiều người mua nhất</h1>
						</div>
						<div class="cap-readmore wow bounceIn" data-wow-duration="1.1s" data-wow-delay=".5s">
							<a href="#">mua ngay</a>
						</div>										
					</div>										
				</div>
			</div>
		</div>
		
		<div class="featured-area pad-60">
			<div class="container">
				<div class="row">
					<div class="section-title">
						<h2>Sản phẩm phổ biến</h2>
						<div class="title-icon">
							<span><i class="fa fa-angle-left"></i> <i class="fa fa-angle-right"></i></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="product-tab">
							<ul class="product-nav" role="tablist">
								<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">phổ biến</a></li>
							</ul>
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="home">
									<div class="row">
										<div class="product-curosel">
											<?php
												$sql = "SELECT * FROM product ORDER BY view DESC LIMIT 4";
												mysqli_set_charset($dbConnect, 'UTF8');
												$result = mysqli_query($dbConnect,$sql);
												if (isset($result) && mysqli_num_rows($result) > 0) {
												while ($row = mysqli_fetch_assoc($result)) {
													$rp= $row['price'] - $row['discount'];
													echo "
														<div class=\"col-md-12\">
													<div class=\"single-product\">
													<div class=\"product-img\">
														<a href=\"product-details.php?id=${row['id']}&view=${row['view']}\">
															<img src=\"${row['image_link']}\" alt=\"w1\" />										
														</a>
														<span class=\"tag-line\">${row['view']} views</span>
														<div class=\"product-action\">
															<div class=\"button-cart\">
																<button><i class=\"fa fa-shopping-cart\"></i>Thêm vào giỏ</button>
															</div>
														</div>
													</div>
													<div class=\"product-content\">
														<h3><a href=\"product-details.php?id=${row['id']}&view=${row['view']}\">${row['name']}</a></h3>
														<div class=\"price\">
															<span>${rp}</span>
															<span class=\"old\">${row['price']}</span>
														</div>
													</div>
													</div>
												</div>
													";
												}
												} 
											?>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>						
		<div class="upcoming-product-area pad-60">
			<div class="container">		
				<div class="row">
					<div class="upcoming-curosel">
						<div class="upcoming-single">
							
							<?php
								$sql = "SELECT * FROM product ORDER BY discount DESC LIMIT 1";
								mysqli_set_charset($dbConnect, 'UTF8');
								$result = mysqli_query($dbConnect,$sql);
								if (isset($result) && mysqli_num_rows($result) > 0) {
								while ($row = mysqli_fetch_assoc($result)){
									$rp= $row['price'] - $row['discount'];
									echo "
										<div class=\"col-md-4 col-sm-4\">
											<div class=\"upcoming-img\">
												<a href=\"product-details.php?id=${row['id']}&view=${row['view']}\"><img src=\"${row['image_link']}\" alt=\"unnamed1-1\" /></a>
											</div>
										</div>
										<div class=\"col-md-8 col-sm-8\">
											<div class=\"upcoming-content\">
												<h2><a href=\"#\">${row['name']}</a></h2>
												<p>${row['content']}</p>
												<div class=\"timer\">
													<div data-countdown=\"2017/08/01\"></div>
												</div>	
												<div class=\"price\">
													<span>${rp} $</span>
													<span class=\"old\">${row['price']} $</span>
												</div>	
												<div class=\"product-action\">
													<div class=\"button-cart\">
														<button><i class=\"fa fa-shopping-cart\"></i> thêm vào giỏ</button>
													</div>
												</div>							
											</div>
										</div>
										";
								}
								}
							?>
						
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="category-area pad-60">
			<div class="container">
				<div class="row">
					<div class="section-title">
						<h2>Giảm giá</h2>
						<div class="title-icon">
							<span><i class="fa fa-angle-left"></i> <i class="fa fa-angle-right"></i></span>
						</div>
					</div>
				</div>			
				<div class="row">
					<div class="product-curosel">
						<?php
						
							$sql = "SELECT * FROM product ORDER BY  LIMIT 8";
							mysqli_set_charset($dbConnect, 'UTF8');
							$result = mysqli_query($dbConnect,$sql);
							if (isset($result) && mysqli_num_rows($result) > 0) {
							while ($row = mysqli_fetch_assoc($result)) {
								$rp= $row['price'] - $row['discount'];
								echo "
									<div class=\"col-md-12\">
										<div class=\"single-product\">
											<div class=\"product-img\">
												<a href=\"product-details.php?id=${row['id']}&view=${row['view']}\">
													<img src=\"${row['image_link']}\" alt=\"w2\" />
												</a>
												<span class=\"tag-line\">Mới</span>
												<div class=\"product-action\">
													<div class=\"button-cart\">
														<button><i class=\"fa fa-shopping-cart\"></i> Thêm vào giỏ</button>
													</div>
												</div>
											</div>
											<div class=\"product-content\">
												<h3><a href=\"product-details.php?id=${row['id']}&view=${row['view']}\">${row['name']}</a></h3>
												<div class=\"price\">
													<span>${rp}</span>
													<span class=\"old\">${row['price']}</span>
												</div>
											</div>
										</div>
									</div>	
										";
									}
								}
							?>
					</div>		
				</div>
			</div>			
		</div>				
		<footer>
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="footer-logo">
								<img src="img/streetwear.png" alt="streetwear" />
								<p>LuxuryStreetWear mang đến cho bạn những bộ sưu tập mới nhất làm điên đảo toàn cầu <br /> Hãy đến với chúng tôi để bắt kịp với xu thế thời trang thế giới</p>
								<div class="widget-icon">
									<a href="https://www.facebook.com/ducphu96bn"><i class="fa fa-facebook"></i></a>
									<a href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-top-area">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="footer-widget">
								<h3>Liên hệ</h3>
								<ul class="footer-contact">
									<li>
										<i class="fa fa-map-marker"> </i>
										Địa chỉ: 8 Mã Mây, Quận Hoàn Kiếm, Hà Nội
									</li>
									<li>
										<i class="fa fa-envelope"> </i>	
										Email: luxurystreetwear@shop.com
									</li>
									<li>
										<i class="fa fa-phone"> </i>
										Điện thoại: 0989.899.998
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 hidden-sm">
							<div class="footer-widget">
								<h3>Tài khoản của tôi</h3>
								<ul class="footer-menu">
									<li><a href="#">Trạng thái đơn hàng</a></li>
									<li><a href="login.php">Đăng kí/Đăng nhập</a></li>
									<li><a href="#">Danh sách mơ ước</a></li>
									<li><a href="#">Kiểu thanh toán</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="footer-widget">
								<h3>Về chúng tôi</h3>
								<ul class="footer-menu">
									<li><a href="#">Giao hàng</a></li>
									<li><a href="#">Thanh toán</a></li>
									<li><a href="#">Thông tin pháp lí</a></li>
									<li><a href="#">Về chúng tôi</a></li>
									<li><a href="#">Liên hệ với chúng tôi</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="footer-widget">
								<h3>Tags</h3>
								<div class="product-tag">
									<ul>
										<li><a href="#">Top</a></li>
										<li><a href="#">Fashion</a></li>
										<li><a href="#">Collection</a></li>
										<li><a href="#">Women</a></li>
										<li><a href="#">men</a></li>
										<li><a href="#">gallery</a></li>
										<li><a href="#">new</a></li>
										<li><a href="#">Collection men</a></li>
										<li><a href="#">Top</a></li>
										<li><a href="#">Fashion</a></li>
										<li><a href="#">Collection</a></li>										
										<li><a href="#">best</a></li>
										<li><a href="shop.php">cloth</a></li>
									</ul>
								</div>
							</div>
						</div>					
					</div>
				</div>
			</div>
			<div class="footer-bottom-area">
				<div class="container">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="payment-img">
								<img src="img/payment.png" alt="payment" />
							</div>
						</div>
					</div>
				</div>
			</div>	
		</footer>

        <script src="js/vendor/jquery-1.12.0.min.js"></script>

        <script src="js/bootstrap.min.js"></script>
	
        <script src="js/owl.carousel.min.js"></script>
	
        <script src="js/jquery.meanmenu.js"></script>
		
        <script src="js/jquery-ui.min.js"></script>
		
		<script src="lib/js/jquery.nivo.slider.pack.js"></script>		
		<script src="lib/js/nivo-active.js"></script>		
		
        <script src="js/wow.min.js"></script>
		
        <script src="js/plugins.js"></script>
	
        <script src="js/main.js"></script>
    </body>
</html>
